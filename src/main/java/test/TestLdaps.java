package test;

import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;

import org.junit.Test;

public class TestLdaps
{
    // This should be a valid ldaps URL of the form:
    // "ldaps://example.com:10636/o=example"
    private static final String LDAPS_URL = "fill me in!";

    private static void ldapsConnect(String classname, String ldapsUrl) throws NamingException
    {
        Hashtable<String, Object> environment = new Hashtable<String, Object>();
        environment.put(Context.SECURITY_PROTOCOL, "ssl");
        environment.put("java.naming.ldap.factory.socket", classname);
        environment.put(Context.INITIAL_CONTEXT_FACTORY,
                "com.sun.jndi.ldap.LdapCtxFactory");
        environment.put(Context.PROVIDER_URL, ldapsUrl);
        environment.put("com.sun.jndi.ldap.connect.timeout", "5000");

        DirContext ctx = new InitialDirContext(environment);

        ctx.lookup("");
    }

    @Test
    public void testWorking() throws NamingException
    {
        ldapsConnect("Working", LDAPS_URL);
    }

    @Test
    public void testBroken() throws NamingException
    {
        // The expected outcome of this test with Java 8u51 or equivalent is:
        // javax.naming.CommunicationException: <LDAPS_URL> [Root exception is javax.net.ssl.SSLHandshakeException: java.security.cert.CertificateException: No subject alternative names present]
        ldapsConnect("Broken", LDAPS_URL);
    }
}
